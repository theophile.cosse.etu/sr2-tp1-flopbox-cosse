# Introduction
Flopbox est une API REST proposant un access a un ensemble de serveur FTP via le protocole HTTP. 

# Lancement de l'application
Installation des dépendances.
```
mvn clean install
```
Compilation
```
mvn clean compile
```
Lancement
```
mvn exec:java
```
# Architecture

La liste des serveur est stocké dans un fichier textes et est mise a jours en temps reel.
Il existe egalement un fichier listant les utilisateur authorisé mais celui-ci est read-only.

Il est necessaire de se connecté une seul fois, le serveur reconnais l'utilisateur grace a son adresse IP

Les fonction gérant les communication avec le serveur FTP sont situées dans la classe FTPResquestHandler.
Les fonction gérant les lien entre alias et serveur FTP sont situées dans la classe FTPServerAlias.
Les fonction gérant l'Authentification dans la classe UserManager.
## AUTHENTICATION

###connect
```
curl  --location --request GET 'http://localhost:8080/flopbox/auth/connect' --header 'id:theophile' --header 'password:1234' 
```
###disconnect
```
curl  --location --request GET 'http://localhost:8080/flopbox/auth/disconnect' 
```

## FTP SERVER

###list ftp servers
```
curl  --request GET 'http://localhost:8080/flopbox/ftp'
```

###add ftp server
```
curl  --request POST 'http://localhost:8080/flopbox/ftp' --header 'name:dltest' --header 'ip:http://speedtest.tele2.net/' --header 'port:21' 
```

###remove ftp server
```
curl  --request DELETE 'http://localhost:8080/flopbox/ftp' --header 'name:dltest'
```

###update ftp server
```
curl --request PUT 'http://localhost:8080/flopbox/ftp' --header 'name:dltest' --header 'ip:http://speedtest.tele2.net/' --header 'port:1234'
```

## FICHIER ET DOSSIERS

###list files
```
curl  --request GET 'http://localhost:8080/flopbox/ftp/univlille/' --header 'id:cosse' --header 'password:____' --header 'action:list'
```

###delete file / directory
```
curl  --location --request DELETE 'http://localhost:8080/flopbox/ftp/univlille/public_html' --header 'id:cosse' --header 'password:_____'
```

###create directory
```
curl   --request GET 'http://localhost:8080/flopbox/ftp/univlille/public_html' --header 'id:cosse' --header 'password:_____' --header 'action:makedir'
```
###rename directory
```
curl  --request PUT 'http://localhost:8080/flopbox/ftp/univlille/repertoire' --header 'id:cosse' --header 'password:_____' --header 'newName:efzlmkj'
```

###upload directory
```
curl --request POST -F 'data=@lol.txt' 'http://localhost:8080/flopbox/ftp/univlille/lol3.txt' --header 'id:cosse' --header 'password:________' --header 'action:upload'
```
###download file
```
curl  --request GET 'http://localhost:8080/flopbox/ftp/univlille/first_diff.jpg' --output first_diff.jpg --header 'id:cosse' --header 'password:____' --header 'action:download'
```

### Lien vidéo de présentation
https://drive.google.com/file/d/1mUw4aa1FEmgVDTd1OkPrkJj82taPdLmq/view?usp=sharing

note : Cette vidéo présente la version qui etait censé etre rendu mercredi.
La version actuel fix les messages d'erreur envoyer des les reponses http et propose  un début d'implementation
de la fonction download.