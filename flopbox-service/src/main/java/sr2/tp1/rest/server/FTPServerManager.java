package sr2.tp1.rest.server;

import sr2.tp1.rest.client.UnknownFTPServerException;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FTPServerManager {

    private static FTPServerManager instance;

    private final HashMap<String,FTPServerData> ftpServerDataHashMap;

    private static final String DATABASE_NAME = "database_ftp.txt";

    private FTPServerManager() {
        this.ftpServerDataHashMap = new HashMap<>();
        try {
            loadFTPServerData();
        } catch (IOException e) {
            System.out.println("Can't access ftp database");
            e.printStackTrace();
        }

    }


    private void loadFTPServerData() throws IOException {
        File file = new File(DATABASE_NAME);
        if(file.exists() && !file.isDirectory()) {
            System.out.println("Database ftp exist loading password list");
        } else {
            System.out.println("Database ftp doesn't exist creating one");
            PrintWriter writer = new PrintWriter(file);
            // fake id / password
            writer.println("webtp.fil.univ-lille1.fr 21 univlille");
            writer.close();
        }
        readFTPServerDataFile(DATABASE_NAME);

    }
    private void readFTPServerDataFile(String filename) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] ipPortName= line.split("\\s+");
                if (ipPortName.length < 3){
                    throw new IOException();
                } else {
                    String ipLowerCase = ipPortName[0].toLowerCase(Locale.ROOT);
                    int port = Integer.parseInt(ipPortName[1]);
                    String nameLowerCase = ipPortName[2].toLowerCase(Locale.ROOT);
                    ftpServerDataHashMap.put(nameLowerCase,new FTPServerData(ipLowerCase,port));
                }

            }
        }
    }

    public boolean deleteServerData(String name){
        if (ftpServerDataHashMap.containsKey(name)){
            ftpServerDataHashMap.remove(name);
            return true;
        }
        return false;
    }


    public boolean addServerData(String name,String ip,String port){
        if (ftpServerDataHashMap.containsKey(name)){
            return false;
        }
        String ipLowerCase = ip.toLowerCase(Locale.ROOT);
        int Intport = Integer.parseInt(port);
        String nameLowerCase = name.toLowerCase(Locale.ROOT);
        ftpServerDataHashMap.put(nameLowerCase,new FTPServerData(ipLowerCase,Intport));
        return true;
    }


    public void saveFTPServerDataFile() throws FileNotFoundException {
        File file = new File(DATABASE_NAME);
        PrintWriter writer = new PrintWriter(file);
        for (Map.Entry mapEntry : ftpServerDataHashMap.entrySet()) {
            FTPServerData ftpServerData = (FTPServerData) mapEntry.getValue();
            writer.println(ftpServerData.getIp() + " " + ftpServerData.getPort() + " " + mapEntry.getKey());
        }
        writer.close();
    }

    public HashMap<String,FTPServerData>  getServerList(){
        return ftpServerDataHashMap;
    }

    public boolean updateServerData(String name,String ip,String port){
        int intPort = Integer.parseInt(port);
        if (ftpServerDataHashMap.containsKey(name)){
            FTPServerData newFTPServerData = new FTPServerData(ip,intPort);
            ftpServerDataHashMap.put(name,newFTPServerData);
            return true;
        } else {
            return false;
        }
    }

    public FTPServerData getServerData(String name) throws UnknownFTPServerException {
        if (ftpServerDataHashMap.containsKey(name)){
            return ftpServerDataHashMap.get(name);
        } else {
            throw new UnknownFTPServerException("Can't find server : "+name);
        }
    }



    public static FTPServerManager getInstance() {
        if (instance == null) {
            instance = new FTPServerManager();
        }
        return instance;
    }

}