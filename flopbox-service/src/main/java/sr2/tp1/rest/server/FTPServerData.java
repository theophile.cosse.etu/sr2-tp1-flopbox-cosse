package sr2.tp1.rest.server;

public class FTPServerData {
    private  String ip;
    private int port;
    public FTPServerData(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }
    public int getPort() {
        return port;
    }

}
