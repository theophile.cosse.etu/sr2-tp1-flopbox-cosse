package sr2.tp1.rest.client;

public class ServerUnreachableException extends Exception{
    public ServerUnreachableException(String message) {
        super(message);
    }
}
