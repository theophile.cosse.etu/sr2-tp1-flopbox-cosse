package sr2.tp1.rest.client;

import org.apache.commons.net.ftp.*;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.InputStream;
import java.net.SocketException;
import java.io.IOException;
/**
 * FTPRequestHandler
 */
public class FTPRequestHandler {

    private FTPClient ftp;
    private FTPClientConfig config;
    private String id;
    private String password;
    private String serverIP;
    private int serverPort;

    public FTPRequestHandler(String id, String password,String serverIP, int serverPort) {
        this.id = id;
        this.password = password;
        this.serverIP = serverIP;
        this.serverPort = serverPort;
        ftp = new FTPClient();
        config = new FTPClientConfig();


    }

    public void connect() throws ServerUnreachableException {
        try {
            ftp.connect(serverIP);
            ftp.login(id, password);
            System.out.println(ftp.getReplyString());
        } catch (IOException e) {
            e.printStackTrace();
            throw new ServerUnreachableException("Can't reach "+ serverIP);
        }

        int reply = ftp.getReplyCode();
        if(!FTPReply.isPositiveCompletion(reply)) {
            try {
                ftp.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
                throw new ServerUnreachableException("Can't reach "+ serverIP);
            }
            System.err.println("FTP server refused connection.");
            throw new ServerUnreachableException("log in "+ serverIP);
        }
    }

    public void logout() throws IOException {
        ftp.logout();
        if(ftp.isConnected()) {
            ftp.disconnect();
        }
    }

    public boolean storeFile(String remote, InputStream local) throws ServerUnreachableException {
        try {
            ftp.enterLocalPassiveMode();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            boolean success = ftp.storeFile(remote,local);
            logout();
            return success;
        } catch (IOException e) {
            e.printStackTrace();
            throw new ServerUnreachableException("Can't reach "+ serverIP);
        }
    }

    public boolean rename(String path, String name) throws ServerUnreachableException {
        try {
            boolean success = ftp.rename(path,name);
            logout();
            return success;
        } catch (IOException e) {
            throw  new ServerUnreachableException("can't reach server " + serverIP);
        }
    }

    public boolean makeDirectory(String path) throws ServerUnreachableException {
        try {
            boolean success = ftp.makeDirectory(path);
            logout();
            return success;
        } catch (IOException e) {
            e.printStackTrace();
            throw  new ServerUnreachableException("can't reach server " + serverIP);
        }
    }

    public boolean delete(String path) throws ServerUnreachableException {
        try {
            // Is a file
            boolean success;
            if (ftp.changeWorkingDirectory(path)){
                ftp.changeWorkingDirectory("/");
                success = ftp.removeDirectory(path);
            } else {
                success = ftp.deleteFile(path);
            }
            logout();
            return success;
        } catch (IOException e) {
            throw  new ServerUnreachableException("can't reach server " + serverIP);
        }
    }
    public StreamingOutput download(final String filePath){
        ftp.enterLocalPassiveMode();
        StreamingOutput fileStream = new StreamingOutput() {
            @Override
            public void write(java.io.OutputStream output) throws IOException, WebApplicationException {
                try {
                    ftp.setFileType(FTP.BINARY_FILE_TYPE);
                    InputStream is = ftp.retrieveFileStream(filePath);
                    byte[] data = is.readAllBytes();
                    output.write(data);
                    output.flush();
                    logout();
                } catch (Exception e) {
                    throw new WebApplicationException(filePath + "not found");
                }
            }
        };
        return fileStream;
    }
    public String listFiles(String path) throws ServerUnreachableException {
        FTPFile[] files;
        try {
            ftp.enterLocalPassiveMode();
            files = ftp.listFiles(path);
            System.out.println(ftp.getReplyString());
            logout();
        } catch (IOException e) {
            throw new ServerUnreachableException("can't reach server " + serverIP);
        }
        StringBuilder builder = new StringBuilder( "ListFiles : \n" );
        for (FTPFile ftpFile: files) {
            builder.append(ftpFile.getName()).append("\n");
        }
        return builder.toString();
    }
    

}