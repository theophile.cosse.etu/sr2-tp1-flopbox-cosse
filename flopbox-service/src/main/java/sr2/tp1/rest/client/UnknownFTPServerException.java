package sr2.tp1.rest.client;

public class UnknownFTPServerException extends Exception{
    public UnknownFTPServerException(String message) {
        super(message);
    }
}
