package sr2.tp1.rest.client;

public class UnknownPathException extends Exception{
    public UnknownPathException(String message) {
        super(message);
    }
}
