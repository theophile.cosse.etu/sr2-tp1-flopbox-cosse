package sr2.tp1.rest.user;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;

/**
 * Cette classe permet de verifier le couple identifier / mot de passe d'un utilisateur et de lui
 * associé une IP. Cette IP peut ensuite etre utiliser par
 *
 * This class handle the id/password checking, create an user with a specific IP.
 *
 */
public class UserManager {

    private static UserManager instance;

    private final HashMap<String,String> IDPasswordMap;
    private final HashMap<String,User> IPUserMap;
    private static final String DATABASE_NAME = "database_id.txt";

    private UserManager() {
        this.IDPasswordMap = new HashMap<>();
        this.IPUserMap = new HashMap<>();
        try {
            loadIDPassword();
        } catch (IOException e) {
            System.out.println("Can't access password database");
            e.printStackTrace();
        }

    }

    /**
     * Create an user if the password and id match and the user doesn't exist already
     * @param id identifier
     * @param password password
     * @param ip ip
     * @return true if the user is already created or can be created, false if wrong id / password
     */
    public boolean connect(String id,String password,String ip){
        String passwordLowerCase = password.toLowerCase(Locale.ROOT);
        String idLowerCase = id.toLowerCase(Locale.ROOT);
        // does the id exist ?
        System.out.println(passwordLowerCase);
        System.out.println(idLowerCase);
        if (IDPasswordMap.containsKey(idLowerCase)){

            int passwordComparison = IDPasswordMap.get(idLowerCase).compareTo(passwordLowerCase);
            // does the password match ?
            if (passwordComparison == 0){
                // is the user already logged in ?
                if (!IPUserMap.containsKey(ip)) {
                    User newUser = new User(idLowerCase,passwordLowerCase,ip);
                    IPUserMap.put(ip,newUser);
                }
                return  true;
            }
        }
        return  false;
    }

    public boolean isConnected(String ip){
        return IPUserMap.containsKey(ip);
    }

    /**
     * Attempt to disconnect a user with a given ip
     * @param ip ip of the user to disconnect
     * @return true if the user can be disconnected false if the ip doesn't correpond to any known user.
     */
    public boolean disconnect(String ip){
        if (IPUserMap.containsKey(ip)) {
            IPUserMap.remove(ip);
            return true;
        } else {
            return false;
        }
    }

    private void loadIDPassword() throws IOException {
        File file = new File(DATABASE_NAME);
        if(file.exists() && !file.isDirectory()) {
            System.out.println("Database exist loading password list");
        } else {
            System.out.println("Database doesn't exist creating one");
            PrintWriter writer = new PrintWriter(file);
            // fake id / password
            writer.println("theophile 1234");
            writer.println("abcd efgh");
            writer.println("aa bb");
            writer.close();
        }
        readIDPasswordFile(DATABASE_NAME);

    }
    private void readIDPasswordFile(String filename) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] idPwd= line.split("\\s+");
                if (idPwd.length < 2){
                    throw new IOException();
                } else {
                    String idLowerCase = idPwd[0].toLowerCase(Locale.ROOT);
                    String passwordLowerCase = idPwd[1].toLowerCase(Locale.ROOT);
                    IDPasswordMap.put(idLowerCase,passwordLowerCase);
                }

            }
        }
    }

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

}
