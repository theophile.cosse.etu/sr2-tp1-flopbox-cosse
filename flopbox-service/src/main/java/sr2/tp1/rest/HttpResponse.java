package sr2.tp1.rest;

public abstract class HttpResponse {

    public static final String CONNECTION_REQUIRED = "Connection required\n";
    public static final String UNKNOWN_SERVER = "Unknown server\n";
    public static final String UNKNOWN_PATH = "Unknown path\n";
    public static final String CANT_REACH_SERVER = "Can't reach server\n";
    public static final String UNKNOWN_ACTION = "Unknown action\n";
    public static final String SERVER_ERROR = "SERVER ERROR\n";
}
