package sr2.tp1.rest.resource;

import org.glassfish.grizzly.http.server.Request;
import sr2.tp1.rest.user.UserManager;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Root resource (exposed at "/" path)
 */
@Path("/auth")
public class Auth {



    @GET
    @Path("/connect")
    @Produces(MediaType.TEXT_PLAIN)
    public Response connect(@Context Request request, @HeaderParam("id") String id, @HeaderParam("password") String password) {
        String ip = request.getRemoteAddr();
        UserManager userManager  = UserManager.getInstance();

        if (userManager.connect(id,password,ip)) {
            return Response.status(200).entity("Welcome "+id+ " ! \n").build();
        } else {
            return Response.status(400).entity("Unknown user / password \n").build();
        }
    }

    @GET
    @Path("/disconnect")
    @Produces(MediaType.TEXT_PLAIN)
    public Response disconnect(@Context Request request) {
        String ip = request.getRemoteAddr();
        UserManager userManager  = UserManager.getInstance();

        if (userManager.disconnect(ip)) {
            return Response.status(200).entity("Disconnect\n").build();
        } else {
            return Response.status(400).entity("You are not logged in\n").build();
        }
    }
}
