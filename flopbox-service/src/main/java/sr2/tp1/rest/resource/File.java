
package sr2.tp1.rest.resource;


import org.glassfish.grizzly.http.server.Request;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import sr2.tp1.rest.client.FTPRequestHandler;
import sr2.tp1.rest.client.ServerUnreachableException;
import sr2.tp1.rest.client.UnknownPathException;
import sr2.tp1.rest.server.FTPServerData;
import sr2.tp1.rest.server.FTPServerManager;
import sr2.tp1.rest.client.UnknownFTPServerException;
import sr2.tp1.rest.user.UserManager;

import javax.ws.rs.*;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static sr2.tp1.rest.HttpResponse.*;

@Path("/ftp/{ftpServer}/{path: .*}")
public class File {

    @PUT
    @Path("")
    @Produces(MediaType.TEXT_PLAIN)
    public Response rename(@Context Request request,
                         @HeaderParam("id") String id,
                         @HeaderParam("password") String password,
                           @HeaderParam("newName") String newName,
                         @PathParam("ftpServer") String ftpServer,
                         @PathParam("path") String path) {
        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        try {
            FTPServerData ftpServerData =  FTPServerManager.getInstance().getServerData(ftpServer);
            FTPRequestHandler ftpRequestHandler = new FTPRequestHandler(id,
                    password,
                    ftpServerData.getIp(),
                    ftpServerData.getPort());
            ftpRequestHandler.connect();


            if(ftpRequestHandler.rename(path,newName)) {
                return Response.status(200).entity("file renamed\n").build();
            } else {
                return Response.status(404).entity("Can't rename file\n").build();
            }
        } catch (UnknownFTPServerException e) {
            return Response.status(404).entity(UNKNOWN_SERVER).build();
        } catch (ServerUnreachableException e) {
            return Response.status(404).entity(CANT_REACH_SERVER).build();
        }
    }

    @DELETE
    @Path("")
    @Produces(MediaType.TEXT_PLAIN)
    public Response delete(@Context Request request,
                           @HeaderParam("id") String id,
                           @HeaderParam("password") String password,
                           @PathParam("ftpServer") String ftpServer,
                           @PathParam("path") String path) {
        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        try {
            FTPServerData ftpServerData =  FTPServerManager.getInstance().getServerData(ftpServer);
            FTPRequestHandler ftpRequestHandler = new FTPRequestHandler(id,
                    password,
                    ftpServerData.getIp(),
                    ftpServerData.getPort());
            ftpRequestHandler.connect();

            if(ftpRequestHandler.delete(path)) {
                return Response.status(200).entity("file/directory deleted\n").build();
            } else {
                return Response.status(404).entity("Can't delete file/directory\n").build();
            }
        } catch (UnknownFTPServerException e) {
            return Response.status(404).entity(UNKNOWN_SERVER).build();
        } catch (ServerUnreachableException e) {
            return Response.status(404).entity(CANT_REACH_SERVER).build();
        }
    }

    @GET
    @Path("")
    @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN })
    public Response getSwitch(@Context Request request,
                               @HeaderParam("id") String id,
                               @HeaderParam("password") String password,
                               @HeaderParam("action") String action,
                               @PathParam("ftpServer") String ftpServer,
                               @PathParam("path") String path) {
        switch(action) {
            case "list":
                return list(request,id,password,ftpServer,path);
            case "download":
                return download(request,id,password,ftpServer,path);
            case "makedir":
                return makeDir(request,id,password,ftpServer,path);
            default:
                return Response.status(404).entity(UNKNOWN_ACTION).build();

        }

    }
    private Response download(Request request,
                         String id,
                         String password,
                         String ftpServer,
                         String path) {
        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        try {
            FTPServerData ftpServerData =  FTPServerManager.getInstance().getServerData(ftpServer);
            FTPRequestHandler ftpRequestHandler = new FTPRequestHandler(id,
                    password,
                    ftpServerData.getIp(),
                    ftpServerData.getPort());
            ftpRequestHandler.connect();

            StreamingOutput fileStream = ftpRequestHandler.download(path);
            System.out.println(path);
            return Response.ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
                    .header("content-disposition", "attachment; filename = " + path).build();
        } catch (UnknownFTPServerException e) {
            return Response.status(404).entity(UNKNOWN_SERVER).build();
        } catch (ServerUnreachableException e) {
            return Response.status(404).entity(CANT_REACH_SERVER).build();
        }
    }


    private Response list(Request request,
                         String id,
                         String password,
                         String ftpServer,
                         String path) {
        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        try {
            FTPServerData ftpServerData =  FTPServerManager.getInstance().getServerData(ftpServer);
            FTPRequestHandler ftpRequestHandler = new FTPRequestHandler(id,
                    password,
                    ftpServerData.getIp(),
                    ftpServerData.getPort());
            ftpRequestHandler.connect();
            String fileList = ftpRequestHandler.listFiles(path);
            return Response.status(200).entity(fileList).build();
        } catch (UnknownFTPServerException e) {
            return Response.status(404).entity(UNKNOWN_SERVER).build();
        } catch (ServerUnreachableException e) {
            return Response.status(404).entity(CANT_REACH_SERVER).build();
        }
    }

    @POST
    @Path("")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public Response upload(@Context Request request,
                           @HeaderParam("id") String id,
                           @HeaderParam("password") String password,
                           @HeaderParam("action") String action,
                           @PathParam("ftpServer") String ftpServer,
                           @PathParam("path") String path,
                               @FormDataParam("data") InputStream fileInputStream) {

        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        try {
            FTPServerData ftpServerData =  FTPServerManager.getInstance().getServerData(ftpServer);
            FTPRequestHandler ftpRequestHandler = new FTPRequestHandler(id,
                    password,
                    ftpServerData.getIp(),
                    ftpServerData.getPort());
            ftpRequestHandler.connect();

            if (ftpRequestHandler.storeFile(path,fileInputStream)) {
                fileInputStream.close();
                return Response.status(200).entity("File uploaded \n").build();
            } else {
                return Response.status(400).entity("Can't upload file\n").build();
            }

        } catch (UnknownFTPServerException e) {
            return Response.status(404).entity(UNKNOWN_SERVER).build();
        } catch (ServerUnreachableException e) {
            return Response.status(404).entity(CANT_REACH_SERVER).build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.status(500).entity(SERVER_ERROR).build();
        }
    }

    private Response makeDir(Request request,
                             String id,
                             String password,
                             String ftpServer,
                             String path) {
        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();
        System.out.println("mmmm");
        try {
            FTPServerData ftpServerData =  FTPServerManager.getInstance().getServerData(ftpServer);
            FTPRequestHandler ftpRequestHandler = new FTPRequestHandler(id,
                    password,
                    ftpServerData.getIp(),
                    ftpServerData.getPort());
            ftpRequestHandler.connect();

            ftpRequestHandler.makeDirectory(path);
            System.out.println("aaaaaaaa");
            if(ftpRequestHandler.makeDirectory(path)) {
                return Response.status(200).entity("directory created").build();
            } else {
                return Response.status(404).entity("Can't create directory").build();
            }
        } catch (UnknownFTPServerException e) {
            return Response.status(404).entity(UNKNOWN_SERVER).build();
        } catch (ServerUnreachableException e) {
            return Response.status(404).entity(CANT_REACH_SERVER).build();
        }
    }

}
