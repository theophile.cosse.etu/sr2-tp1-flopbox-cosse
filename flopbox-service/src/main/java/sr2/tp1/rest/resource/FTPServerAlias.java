package sr2.tp1.rest.resource;

import org.glassfish.grizzly.http.server.Request;
import sr2.tp1.rest.server.FTPServerData;
import sr2.tp1.rest.server.FTPServerManager;
import sr2.tp1.rest.user.UserManager;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

import static sr2.tp1.rest.HttpResponse.CONNECTION_REQUIRED;

/**
 * Root resource (exposed at "/" path)
 */
@Path("/ftp")
public class FTPServerAlias {

    @PUT
    @Path("")
    @Produces(MediaType.TEXT_PLAIN)
    public Response update(@Context Request request,
                                    @HeaderParam("name") String name,
                                    @HeaderParam("ip") String ip,
                                    @HeaderParam("port") String port) {

        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        if (FTPServerManager.getInstance().updateServerData(name, ip, port)){
            return Response.status(200).entity("Server updated\n").build();
        }
        return Response.status(401).entity("This server does not exist\n").build();
    }

    @POST
    @Path("")
    @Produces(MediaType.TEXT_PLAIN)
    public Response add(@Context Request request,
                                 @HeaderParam("name") String name,
                                 @HeaderParam("ip") String ip,
                                 @HeaderParam("port") String port) {

        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        if (FTPServerManager.getInstance().addServerData(name, ip, port)){
            return Response.status(201).entity("Server added\n").build();
        }
        return Response.status(401).entity("This server already exist\n").build();
    }

    @DELETE
    @Path("")
    @Produces(MediaType.TEXT_PLAIN)
    public Response delete(@Context Request request,@HeaderParam("name") String name) {
        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();


        if (FTPServerManager.getInstance().deleteServerData(name)){
            return Response.status(200).entity("Server removed\n").build();
        }
        return Response.status(401).entity("This server does not exist\n").build();
    }


    @GET
    @Path("")
    @Produces(MediaType.TEXT_PLAIN)
    public Response list(@Context Request request) {
        if(!UserManager.getInstance().isConnected(request.getRemoteAddr()))
            return Response.status(401).entity(CONNECTION_REQUIRED).build();

        StringBuilder builder = new StringBuilder( "Serveur : \n" );
        HashMap<String, FTPServerData> ftpServerList = FTPServerManager.getInstance().getServerList();
        for (Map.Entry mapEntry : ftpServerList.entrySet()) {
            FTPServerData ftpServerData = (FTPServerData) mapEntry.getValue();
            builder.append(mapEntry.getKey()).append(" ").append(ftpServerData.getIp()).append(":");
            builder.append(ftpServerData.getPort()).append("\n");
        }
        return Response.status(200).entity(builder.toString()).build();
    }

}